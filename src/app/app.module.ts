import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FormComponent } from './components/form/form.component';
import { LightboxComponent } from './components/lightbox/lightbox.component';
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FormComponent,
    LightboxComponent,
    FooterComponent
  ],
  imports: [BrowserModule, ReactiveFormsModule, NgSelectModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
