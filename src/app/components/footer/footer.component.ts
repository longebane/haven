import { Component, OnInit } from '@angular/core';
import { LightboxService } from '../../services/lightbox.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  constructor(private lightboxService: LightboxService) {}

  ngOnInit() {}

  openLightbox(id: string) {
    this.lightboxService.open(id);
  }

  closeLightbox(id: string) {
    this.lightboxService.close(id);
  }
}
