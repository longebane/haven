import { Component, ElementRef, Input, OnInit, OnDestroy } from '@angular/core';

import { LightboxService } from '../../services/lightbox.service';

@Component({
  selector: 'app-lightbox',
  templateUrl: './lightbox.component.html',
  styleUrls: ['./lightbox.component.scss']
})
export class LightboxComponent implements OnInit, OnDestroy {
  @Input() id: string;
  private element: any;

  constructor(
    private lightboxService: LightboxService,
    private el: ElementRef
  ) {
    this.element = el.nativeElement;
  }

  ngOnInit(): void {
    let lightbox = this;

    //hide lightbox on init
    this.element.style.display = 'none';

    //ensure id attribute exists
    if (!this.id) {
      return;
    }

    //display above everything
    document.body.appendChild(this.element);

    //check for bg clicks
    this.element.addEventListener('click', function(e: any) {
      if (e.target.className === 'lightbox') {
        lightbox.close();
      }
    });

    this.lightboxService.add(this);
  }

  ngOnDestroy(): void {
    this.lightboxService.remove(this.id);
    this.element.remove();
  }

  open(): void {
    this.element.style.display = 'block';
  }

  close(): void {
    this.element.style.display = 'none';
  }
}
