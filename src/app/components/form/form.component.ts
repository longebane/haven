import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailValidator } from './email-validator.validator';
import { LightboxService } from '../../services/lightbox.service';
import { NgOption } from '@ng-select/ng-select';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  userForm: FormGroup;
  submitted: boolean = false;
  success: boolean = false;
  cities: NgOption[] = [
    { id: 1, name: 'Los Angeles' },
    { id: 2, name: 'New York' },
    { id: 3, name: 'San Francisco' },
    { id: 4, name: 'Atlanta' },
    { id: 5, name: 'Portland' }
  ];

  constructor(
    private formBuilder: FormBuilder,
    private lightboxService: LightboxService
  ) { }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      confirmEmail: [
        '',
        [Validators.required, Validators.email, EmailValidator('email')]
      ],
      metro: [null, Validators.required],
      zipCode: ['', Validators.required],
      phone1: [
        '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(3),
          Validators.pattern('^[0-9]*$')
        ]
      ],
      phone2: [
        '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(3),
          Validators.pattern('^[0-9]*$')
        ]
      ],
      phone3: [
        '',
        [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(4),
          Validators.pattern('^[0-9]*$')
        ]
      ],
      rules: [false, Validators.requiredTrue],
      offers: [false],
      info: [false]
    });
  }

  onSubmit() {
    this.submitted = true;

    if (this.userForm.invalid) {
      return;
    }

    this.success = true;
  }

  onInputEntry(event, nextInput) {
    let input = event.target;
    let length = input.value.length;
    let maxLength = input.attributes.maxlength.value;

    if (length >= maxLength) {
      nextInput.focus();
    }
  }

  openLightbox(id: string) {
    this.lightboxService.open(id);
  }

  closeLightbox(id: string) {
    this.lightboxService.close(id);
  }
}
