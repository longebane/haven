import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LightboxService {
  private lightboxes: any[] = [];

  add(lightbox: any) {
    this.lightboxes.push(lightbox);
  }

  remove(id: string) {
    this.lightboxes = this.lightboxes.filter(x => x.id !== id);
  }

  open(id: string) {
    let lightbox: any = this.lightboxes.filter(x => x.id === id)[0];
    lightbox.open();
  }

  close(id: string) {
    let lightbox: any = this.lightboxes.filter(x => x.id === id)[0];
    lightbox.close();
  }
}
